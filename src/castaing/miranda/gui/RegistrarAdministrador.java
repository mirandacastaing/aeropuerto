package castaing.miranda.gui;

import castaing.miranda.ui.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.net.URL;
import java.util.ResourceBundle;

public class RegistrarAdministrador implements Initializable {

    @FXML
    private TextField nombre, apellidos, identificacion, correo, nacionalidad;

    @FXML
    private TextArea direccion;

    @FXML
    private ChoiceBox<String> generos;

    @FXML
    private DatePicker fechaNacimiento;

    double x = 0, y = 0;

    @FXML
    void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    void registrarUsuario(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("RegistrarUsuario.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void registrarMiembroTripulacion(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("RegistrarMiembroTripulacion.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void registrarAdministrador(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("RegistrarAdministradorMenu.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void enviarInfoAdministradorPrincipio(MouseEvent event) throws Exception {
        Main.recibirInfoAdministrador(nombre.getText(), apellidos.getText(), identificacion.getText(), correo.getText(),
                direccion.getText(), nacionalidad.getText(), fechaNacimiento.getValue().toString(), generos.getValue());
        RegistrarAeropuertoPrincipio.correoAdministrador = correo.getText();
        registrarAeropuerto(event, "RegistrarAeropuertoPrincipio.fxml");
    }

    @FXML
    void enviarInfoAdministrador(MouseEvent event) throws Exception {
        if (!Main.verificarUsuario(correo.getText(), identificacion.getText())) {
            Main.recibirInfoAdministrador(nombre.getText(), apellidos.getText(), identificacion.getText(), correo.getText(),
                    direccion.getText(), nacionalidad.getText(), fechaNacimiento.getValue().toString(), generos.getValue());
            RegistrarAeropuerto.correoAdministrador = correo.getText();
            registrarAeropuerto(event, "RegistrarAeropuertoMenu.fxml");
        }
    }

    @FXML
    void registrarAeropuerto(MouseEvent event, String fxml) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource(fxml)));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void iniciarSesion(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("IniciarSesion.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        listaGeneros.add("Masculino");
        listaGeneros.add("Femenino");
        generos.setItems(listaGeneros);
        generos.getSelectionModel().selectFirst();
    }

    private ObservableList<String> listaGeneros = FXCollections.observableArrayList();

}
