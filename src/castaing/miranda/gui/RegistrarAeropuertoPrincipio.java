package castaing.miranda.gui;

import castaing.miranda.ui.Main;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import java.net.URL;
import java.util.ResourceBundle;

public class RegistrarAeropuertoPrincipio implements Initializable {

    public static String correoAdministrador;

    @FXML
    private TextField nombreAeropuerto, codigoAeropuerto, codigoPais, nombrePais, abreviaturaPais;

    double x = 0, y = 0;

    @FXML
    void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    void enviarInfoAeropuertoPais(MouseEvent event) throws Exception {
        Main.recibirInfoAeropuertoPais(nombreAeropuerto.getText(), codigoAeropuerto.getText(), codigoPais.getText(),
                nombrePais.getText(), abreviaturaPais.getText(), correoAdministrador);
        iniciarSesion(event);
    }

    @FXML
    void iniciarSesion(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("IniciarSesion.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

}
