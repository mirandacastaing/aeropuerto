package castaing.miranda.gui;

import castaing.miranda.ui.Main;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.net.URL;
import java.util.Collections;
import java.util.ResourceBundle;

public class RegistrarUsuario implements Initializable {

    @FXML
    private TextField nombre, apellidos, identificacion, correo, nacionalidad;

    @FXML
    private DatePicker fechaNacimiento;

    @FXML
    private ChoiceBox<String> provincias, cantones, distritos;

    @FXML
    private TextArea direccion;

    double x = 0, y = 0;

    @FXML
    void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    void registrarUsuario(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("RegistrarUsuario.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void registrarMiembroTripulacion(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("RegistrarMiembroTripulacion.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void registrarAdministrador(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("RegistrarAdministradorMenu.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void enviarInfoUsuario(MouseEvent event) throws Exception {
        if (!Main.verificarUsuario(correo.getText(), identificacion.getText())) {
            Main.recibirInfoUsuario(nombre.getText(), apellidos.getText(), identificacion.getText(), correo.getText(),
                    direccion.getText(), nacionalidad.getText(), provincias.getValue(), cantones.getValue(),
                    distritos.getValue(), fechaNacimiento.getValue().toString());
            iniciarSesion(event);
        }
    }

    @FXML
    void iniciarSesion(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("IniciarSesion.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        listaProvincias.add("San José");
        listaProvincias.add("Alajuela");
        listaProvincias.add("Cartago");
        listaProvincias.add("Heredia");
        listaProvincias.add("Puntarenas");
        listaProvincias.add("Guanacaste");
        listaProvincias.add("Limón");
        provincias.setItems(listaProvincias);
        provincias.getSelectionModel().selectFirst();
        popularCantones();
        popularDistritos();
        provincias.getSelectionModel()
                .selectedItemProperty()
                .addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    listaCantones.clear();
                    listaDistritos.clear();
                    popularCantones();
                });
        cantones.getSelectionModel()
                .selectedItemProperty()
                .addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    listaDistritos.clear();
                    if (cantones.getValue() != null) {
                        popularDistritos();
                    }
                });
    }

    public void popularCantones() {
        if (provincias.getValue().equals("San José")) {
            listaCantones.addAll("San José", "Escazú", "Desamparados", "Puriscal", "Tarrazú", "Aserrí", "Mora", "Goicoechea", "Santa Ana", "Alajuelita", "Vásquez de Coronado", "Acosta", "Tibás", "Moravia", "Montes de Oca", "Turrubares", "Dota", "Curridabat", "Pérez Zeledón", "León Cortés Castro");
        } else if (provincias.getValue().equals("Alajuela")) {
            listaCantones.addAll("Alajuela", "San Ramón", "Grecia", "San Mateo", "Atenas", "Naranjo", "Palmares", "Poás", "Orotina", "San Carlos", "Zarcero", "Valverde Vega", "Upala", "Los Chiles", "Guatuso", "Río Cuarto");
        } else if (provincias.getValue().equals("Cartago")) {
            listaCantones.addAll("Cartago", "Paraíso", "La Unión", "Jiménez", "Turrialba", "Alvarado", "Oreamuno", "El Guarco");
        } else if (provincias.getValue().equals("Heredia")) {
            listaCantones.addAll("Heredia", "Barva", "Santo Domingo", "Santa Bárbara", "San Rafael", "San Isidro", "Belén", "Flores", "San Pablo", "Sarapiquí");
        } else if (provincias.getValue().equals("Puntarenas")) {
            listaCantones.addAll("Puntarenas", "Esparza", "Buenos Aires", "Montes de Oro", "Osa", "Quepos", "Golfito", "Coto Brus", "Parrita", "Corredores", "Garabito");
        } else if (provincias.getValue().equals("Guanacaste")) {
            listaCantones.addAll("Liberia", "Nicoya", "Santa Cruz", "Bagaces", "Carrillo", "Cañas", "Abangares", "Tilarán", "Nandayure", "La Cruz", "Hojancha");
        } else if (provincias.getValue().equals("Limón")) {
            listaCantones.addAll("Limón", "Pococí", "Siquirres", "Talamanca", "Matina", "Guácimo");
        }
        cantones.setItems(listaCantones);
        cantones.getSelectionModel().selectFirst();
    }

    public void popularDistritos() {
        if (cantones.getValue().equals("San José")) {
            listaDistritos.addAll("Carmen", "Merced", "Hospital", "Catedral", "Zapote", "San Francisco de Dos Ríos", "Uruca", "Mata Redonda", "Pavas", "Hatillo", "San Sebastián");
        } else if (cantones.getValue().equals("Escazú")) {
            listaDistritos.addAll("Escazú", "San Antonio", "San Rafael");
        } else if (cantones.getValue().equals("Desamparados")) {
            listaDistritos.addAll("Desamparados", "San Miguel", "San Juan de Dios", "San Rafael Arriba", "San Antonio", "Frailes", "Patarrá", "San Cristóbal", "Rosario", "Damas", "San Rafael Abajo", "Gravilias", "Los Guido");
        } else if (cantones.getValue().equals("Puriscal")) {
            listaDistritos.addAll("Santiago", "Mercedes Sur", "Barbacoas", "Grifo Alto", "San Rafael", "Candelarita", "Desamparaditos", "San Antonio", "Chires");
        } else if (cantones.getValue().equals("Tarrazú")) {
            listaDistritos.addAll("San Marcos", "San Lorenzo", "San Carlos");
        } else if (cantones.getValue().equals("Aserrí")) {
            listaDistritos.addAll("Aserrí", "Tarbaca", "Vuelta de Jorco", "San Gabriel", "Legua", "Monterrey", "Salitrillos");
        } else if (cantones.getValue().equals("Mora")) {
            listaDistritos.addAll("Colón", "Guayabo", "Tabarcia", "Piedras Negras", "Picagres", "Jaris");
        } else if (cantones.getValue().equals("Goicoechea")) {
            listaDistritos.addAll("Guadalupe", "San Francisco", "Calle Blancos", "Mata de Plátano", "Ipís", "Rancho Redondo", "Purral");
        } else if (cantones.getValue().equals("Santa Ana")) {
            listaDistritos.addAll("Santa Ana", "Salitral", "Pozos", "Uruca", "Piedades", "Brasil");
        } else if (cantones.getValue().equals("Alajuelita")) {
            listaDistritos.addAll("Alajuelita", "San Josecito", "San Antonio", "Concepción", "San Felipe");
        } else if (cantones.getValue().equals("Vásquez de Coronado")) {
            listaDistritos.addAll("San Isidro", "San Rafael", "Dulce Nombre de Jesús", "Patalillo", "Cascajal");
        } else if (cantones.getValue().equals("Acosta")) {
            listaDistritos.addAll("San Ignacio", "Guaitil", "Palmichal", "Cangrejal", "Sabanillas");
        } else if (cantones.getValue().equals("Tibás")) {
            listaDistritos.addAll("San Juan", "Cinco Esquinas", "Anselmo Llorente", "León XIII", "Colima");
        } else if (cantones.getValue().equals("Moravia")) {
            listaDistritos.addAll("San Vicente", "La Trinidad", "San Jerónimo");
        } else if (cantones.getValue().equals("Montes de Oca")) {
            listaDistritos.addAll("San Pedro", "Sabanilla", "Mercedes", "San Rafael");
        } else if (cantones.getValue().equals("Turrubares")) {
            listaDistritos.addAll("San Pablo", "San Pedro", "San Juan de Mata", "San Luis", "Carara");
        } else if (cantones.getValue().equals("Dota")) {
            listaDistritos.addAll("Santa María", "Jardín", "Copey");
        } else if (cantones.getValue().equals("Curridabat")) {
            listaDistritos.addAll("Curridabat", "Granadilla", "Sánchez", "Tirrases");
        } else if (cantones.getValue().equals("Pérez Zeledón")) {
            listaDistritos.addAll("San Isidro", "General Viejo", "Daniel Flores", "Rivas", "San Pedro", "Platanares", "Pejibaye", "Cajón", "Barú", "Río Nuevo", "Páramo");
        } else if (cantones.getValue().equals("León Cortés Castro")) {
            listaDistritos.addAll("San Pablo", "San Andrés", "Llano Bonito", "San Isidro", "Santa Cruz", "San Antonio");
        } else if (cantones.getValue().equals("Alajuela")) {
            listaDistritos.addAll("Alajuela", "San José", "Carrizal", "San Antonio", "Guácima", "San Isidro", "Sabanilla", "San Rafael", "Río Segundo", "Desamparados", "Turrúcares", "Tambor", "Garita", "Sarapiquí");
        } else if (cantones.getValue().equals("San Ramón")) {
            listaDistritos.addAll("San Ramón", "Santiago", "San Juan", "Piedades Norte", "Piedades Sur", "San Rafael", "San Isidro", "Los Ángeles", "Alfaro", "Volio", "Concepción", "Zapotal", "Peñas Blancas");
        } else if (cantones.getValue().equals("Grecia")) {
            listaDistritos.addAll("Grecia", "San Isidro", "San José", "San Roque", "Tacares", "Río Cuarto", "Puente de Piedra", "Bolívar");
        } else if (cantones.getValue().equals("San Mateo")) {
            listaDistritos.addAll("San Mateo", "Desmonte", "Jesús María", "Labrador");
        } else if (cantones.getValue().equals("Atenas")) {
            listaDistritos.addAll("Atenas", "Jesús", "Mercedes", "San Isidro", "Concepción", "San José", "Santa Eulalia", "Escobal");
        } else if (cantones.getValue().equals("Naranjo")) {
            listaDistritos.addAll("Naranjo", "San Miguel", "San José", "Cirrí Sur", "San Jerónimo", "San Juan", "El Rosario", "Palmitos");
        } else if (cantones.getValue().equals("Palmares")) {
            listaDistritos.addAll("Palmares", "Zaragoza", "Buenos Aires", "Santiago", "Candelaria", "Esquipulas", "La Granja");
        } else if (cantones.getValue().equals("Poás")) {
            listaDistritos.addAll("San Pedro", "San Juan", "San Rafael", "Carrillos", "Sabana Redonda");
        } else if (cantones.getValue().equals("Orotina")) {
            listaDistritos.addAll("Orotina", "Mastate", "Hacienda Vieja", "Coyolar", "La Ceiba");
        } else if (cantones.getValue().equals("San Carlos")) {
            listaDistritos.addAll("Quesada", "Florencia", "Buenavista", "Aguas Zarcas", "Venecia", "Pital", "La Fortuna", "La Tigra", "La Palmera", "Venado", "Cutris", "Monterrey", "Pocosol");
        } else if (cantones.getValue().equals("Zarcero")) {
            listaDistritos.addAll("Zarcero", "Laguna", "Tapesco", "Guadalupe", "Palmira", "Zapote", "Brisas");
        } else if (cantones.getValue().equals("Valverde Vega")) {
            listaDistritos.addAll("Sarchí Norte", "Sarchí Sur", "Toro Amarillo", "San Pedro", "Rodríguez");
        } else if (cantones.getValue().equals("Upala")) {
            listaDistritos.addAll("Upala", "Aguas Claras", "San José O Pizote", "Bijagua", "Delicias", "Dos Ríos", "Yoliyllal", "Canalete");
        } else if (cantones.getValue().equals("Los Chiles")) {
            listaDistritos.addAll("Los Chiles", "Caño Negro", "El Amparo", "San Jorge");
        } else if (cantones.getValue().equals("Guatuso")) {
            listaDistritos.addAll("San Rafael", "Buenavista", "Cote", "Katira");
        } else if (cantones.getValue().equals("Río Cuarto")) {
            listaDistritos.addAll("Río Cuarto");
        } else if (cantones.getValue().equals("Cartago")) {
            listaDistritos.addAll("Oriental", "Occidental", "Carmen", "San Nicolás", "Aguacaliente", "Guadalupe", "Corralillo", "Tierra Blanca", "Dulce Nombre", "Llano Grande", "Quebradilla");
        } else if (cantones.getValue().equals("Paraíso")) {
            listaDistritos.addAll("Paraíso", "Santiago", "Orosi", "Cachí", "Llanos de Santa Lucía");
        } else if (cantones.getValue().equals("La Unión")) {
            listaDistritos.addAll("Tres Ríos", "San Diego", "San Juan", "San Rafael", "Concepción", "Dulce Nombre", "San Ramón", "Río Azul");
        } else if (cantones.getValue().equals("Jiménez")) {
            listaDistritos.addAll("Juan Viñas", "Tucurrique", "Pejibaye");
        } else if (cantones.getValue().equals("Turrialba")) {
            listaDistritos.addAll("Turrialba", "La Suiza", "Peralta", "Santa Cruz", "Santa Teresita", "Pavones", "Tuis", "Tayutic", "Santa Rosa", "Tres Equis", "La Isabel", "Chirripó");
        } else if (cantones.getValue().equals("Alvarado")) {
            listaDistritos.addAll("Pacayas", "Cervantes", "Capellades");
        } else if (cantones.getValue().equals("Oreamuno")) {
            listaDistritos.addAll("San Rafael", "Cot", "Potrero Cerrado", "Cipreses", "Santa Rosa");
        } else if (cantones.getValue().equals("El Guarco")) {
            listaDistritos.addAll("El Tejar", "San Isidro", "Tobosi", "Patio de Agua");
        } else if (cantones.getValue().equals("Heredia")) {
            listaDistritos.addAll("Heredia", "Mercedes", "San Francisco", "Ulloa", "Varablanca");
        } else if (cantones.getValue().equals("Barva")) {
            listaDistritos.addAll("Barva", "San Pedro", "San Pablo", "San Roque", "Santa Lucía", "San Jose de la Montaña");
        } else if (cantones.getValue().equals("Santo Domingo")) {
            listaDistritos.addAll("Santo Domingo", "San Vicente", "San Miguel", "Paracito", "Santo Tomás", "Santa Rosa", "Tures", "Pará");
        } else if (cantones.getValue().equals("Santa Bárbara")) {
            listaDistritos.addAll("Santa Bárbara", "San Pedro", "San Juan", "Jesús", "Santo Domingo", "Purabá");
        } else if (cantones.getValue().equals("San Rafael")) {
            listaDistritos.addAll("San Rafael", "San Josecito", "Santiago", "Los Ángeles", "Concepción");
        } else if (cantones.getValue().equals("San Isidro")) {
            listaDistritos.addAll("San Isidro", "San José", "Concepción", "San Francisco");
        } else if (cantones.getValue().equals("Belén")) {
            listaDistritos.addAll("San Antonio", "La Ribera", "La Asunción");
        } else if (cantones.getValue().equals("Flores")) {
            listaDistritos.addAll("San Joaquín", "Barrantes", "Llorente");
        } else if (cantones.getValue().equals("San Pablo")) {
            listaDistritos.addAll("Sin distritos");
        } else if (cantones.getValue().equals("Sarapiquí")) {
            listaDistritos.addAll("Puerto Viejo", "La Virgen", "Horquetas", "Llanuras del Gaspar", "Cureña");
        } else if (cantones.getValue().equals("Puntarenas")) {
            listaDistritos.addAll("Puntarenas", "Pitahaya", "Chomes", "Lepanto", "Paquera", "Manzanillo", "Guacimal", "Barranca", "Monteverde", "Isla de Coco", "Cóbano", "Chacarita", "Isla Chira", "Acapulco", "El Roble", "Arancibia");
        } else if (cantones.getValue().equals("Esparza")) {
            listaDistritos.addAll("Espíritu Santo", "San Juan Grande", "Macacona", "San Rafael", "San Jerónimo", "Caldera");
        } else if (cantones.getValue().equals("Buenos Aires")) {
            listaDistritos.addAll("Buenos Aires", "Volcán", "Potrero Grande", "Boruca", "Pilas", "Colinas", "Chánguena", "Biolley", "Brunka");
        } else if (cantones.getValue().equals("Montes de Oro")) {
            listaDistritos.addAll("Miramar", "La Unión", "San Isidro");
        } else if (cantones.getValue().equals("Osa")) {
            listaDistritos.addAll("Cortés", "Palmar", "Sierpe", "Bahía Ballena", "Piedras Blancas", "Bahía Drake");
        } else if (cantones.getValue().equals("Quepos")) {
            listaDistritos.addAll("Quepos", "Savegre", "Naranjito");
        } else if (cantones.getValue().equals("Golfito")) {
            listaDistritos.addAll("Golfito", "Puerto Jiménez", "Guaycará", "Pavón");
        } else if (cantones.getValue().equals("Coto Brus")) {
            listaDistritos.addAll("San Vito", "Sabalito", "Agua Buena", "Limoncito", "Pittier", "Gutíerrez Braun");
        } else if (cantones.getValue().equals("Parrita")) {
            listaDistritos.addAll("Sin distritos");
        } else if (cantones.getValue().equals("Corredores")) {
            listaDistritos.addAll("Corridor", "La Cuesta de Corredores", "Canoas de Corredores", "Laurel de Corredores");
        } else if (cantones.getValue().equals("Garabito")) {
            listaDistritos.addAll("Jacó", "Tárcoles");
        } else if (cantones.getValue().equals("Liberia")) {
            listaDistritos.addAll("Liberia", "Cañas Dulces", "Mayorga", "Nacascolo", "Curubandé");
        } else if (cantones.getValue().equals("Nicoya")) {
            listaDistritos.addAll("Nicoya", "Mansión", "San Antonio", "Quebrada Honda", "Sámara", "Nosara", "Belén de Nosarita");
        } else if (cantones.getValue().equals("Santa Cruz")) {
            listaDistritos.addAll("Santa Cruz", "Bolsón", "Veintisiete de Abril", "Tempate", "Cartagena", "Cuajiniquil", "Diriá", "Cabo Velas", "Tamarindo");
        } else if (cantones.getValue().equals("Bagaces")) {
            listaDistritos.addAll("Bagaces", "La Fortuna", "Mogote", "Río Naranjo");
        } else if (cantones.getValue().equals("Carrillo")) {
            listaDistritos.addAll("Filadelfia", "Palmira", "Sardinal", "Belén");
        } else if (cantones.getValue().equals("Cañas")) {
            listaDistritos.addAll("Cañas", "Palmira", "San Miguel", "Bebedero", "Porozal");
        } else if (cantones.getValue().equals("Abangares")) {
            listaDistritos.addAll("Juntas", "Sierra", "San Juan", "Colorado");
        } else if (cantones.getValue().equals("Tilarán")) {
            listaDistritos.addAll("Tilarán", "Quebrada Grande", "Tronadora", "Santa Rosa", "Líbano", "Tierras Morenas", "Arenal");
        } else if (cantones.getValue().equals("Nandayure")) {
            listaDistritos.addAll("Carmona", "Santa Rita", "Zapotal", "San Pablo", "Porvenir", "Bejuco");
        } else if (cantones.getValue().equals("La Cruz")) {
            listaDistritos.addAll("La Cruz", "Santa Cecilia", "La Garita", "Santa Elena");
        } else if (cantones.getValue().equals("Hojancha")) {
            listaDistritos.addAll("Hojancha", "Monte Romo", "Puerto Carrillo", "Huacas");
        } else if (cantones.getValue().equals("Limón")) {
            listaDistritos.addAll("Limón", "Valle La Estrella", "Río Blanco", "Matama");
        } else if (cantones.getValue().equals("Pococí")) {
            listaDistritos.addAll("Guápiles", "Jiménez", "La Rita", "Roxana", "Cariari", "Colorado", "La Colonia");
        } else if (cantones.getValue().equals("Siquirres")) {
            listaDistritos.addAll("Siquirres", "Pacuarito", "Florida", "Germania", "Cairo", "Alegría");
        } else if (cantones.getValue().equals("Talamanca")) {
            listaDistritos.addAll("Bratsi", "Sixaola", "Cahuita", "Telire");
        } else if (cantones.getValue().equals("Matina")) {
            listaDistritos.addAll("Matina", "Batán", "Carrandi");
        } else if (cantones.getValue().equals("Guácimo")) {
            listaDistritos.addAll("Guácimo", "Mercedes", "Pocora", "Río Jiménez", "Duacarí");
        }
        distritos.setItems(listaDistritos);
        distritos.getSelectionModel().selectFirst();
    }

    private ObservableList<String> listaProvincias = FXCollections.observableArrayList();
    private ObservableList<String> listaCantones = FXCollections.observableArrayList();
    private ObservableList<String> listaDistritos = FXCollections.observableArrayList();

}
