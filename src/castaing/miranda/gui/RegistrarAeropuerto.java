package castaing.miranda.gui;

import castaing.miranda.ui.Main;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import java.net.URL;
import java.util.ResourceBundle;

public class RegistrarAeropuerto implements Initializable {

    public static String correoAdministrador;

    @FXML
    private TextField nombreAeropuerto, codigoAeropuerto, codigoPais, nombrePais, abreviaturaPais;

    @FXML
    private ChoiceBox<String> paises;

    String paisSeleccionado;

    double x = 0, y = 0;

    @FXML
    void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    void registrarUsuario(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("RegistrarUsuario.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void registrarMiembroTripulacion(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("RegistrarMiembroTripulacion.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void registrarAdministrador(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("RegistrarAdministradorMenu.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void enviarInfoAeropuerto(MouseEvent event) throws Exception {
        if (paises.getValue().equals("Seleccione un país")) {
            Main.recibirInfoAeropuertoPais(nombreAeropuerto.getText(), codigoAeropuerto.getText(), codigoPais.getText(),
                    nombrePais.getText(), abreviaturaPais.getText(), correoAdministrador);
        } else {
            Main.recibirInfoAeropuerto(nombreAeropuerto.getText(), codigoAeropuerto.getText(), nombrePais.getText(),
                    correoAdministrador);
        }
        iniciarSesion(event);
    }

    @FXML
    void agregarInfoAeropuerto(MouseEvent event) throws Exception {
        if (paises.getValue().equals("Seleccione un país")) {
            Main.recibirInfoAeropuertoPais(nombreAeropuerto.getText(), codigoAeropuerto.getText(), codigoPais.getText(),
                    nombrePais.getText(), abreviaturaPais.getText(), correoAdministrador);
        } else {
            Main.recibirInfoAeropuerto(nombreAeropuerto.getText(), codigoAeropuerto.getText(), paises.getValue(),
                    correoAdministrador);
        }
        listarVuelos(event);
    }

    @FXML
    void listarVuelos(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarVuelos.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void iniciarSesion(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("IniciarSesion.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        String[][] infoPaises = Main.enviarListaPaises();
        listaPaises.add("Seleccione un país");
        for (int i = 0; i < infoPaises.length; i++) {
            listaPaises.add(infoPaises[i][1]);
        }
        paises.setItems(listaPaises);
        paises.getSelectionModel().selectFirst();
        paises.getSelectionModel()
                .selectedItemProperty()
                .addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                if (!paises.getValue().equals("Seleccione un país")) {
                    codigoPais.setDisable(true);
                    nombrePais.setDisable(true);
                    abreviaturaPais.setDisable(true);
                } else {
                    codigoPais.setDisable(false);
                    nombrePais.setDisable(false);
                    abreviaturaPais.setDisable(false);
                }
                });
        codigoPais.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!(codigoPais.getText().equals("") && nombrePais.getText().equals("") && abreviaturaPais.getText().equals(""))) {
                paisSeleccionado = paises.getValue();
                paises.setDisable(true);
                paises.setValue("Seleccione un país");
            } else {
                paises.setDisable(false);
                paises.setValue(paisSeleccionado);
            }
        });
        nombrePais.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!(codigoPais.getText().equals("") && nombrePais.getText().equals("") && abreviaturaPais.getText().equals(""))) {
                paisSeleccionado = paises.getValue();
                paises.setDisable(true);
                paises.setValue("Seleccione un país");
            } else {
                paises.setDisable(false);
                paises.setValue(paisSeleccionado);
            }
        });
        abreviaturaPais.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!(codigoPais.getText().equals("") && nombrePais.getText().equals("") && abreviaturaPais.getText().equals(""))) {
                paisSeleccionado = paises.getValue();
                paises.setDisable(true);
                paises.setValue("Seleccione un país");
            } else {
                paises.setDisable(false);
                paises.setValue(paisSeleccionado);
            }
        });
    }

    private ObservableList<String> listaPaises = FXCollections.observableArrayList();

}
