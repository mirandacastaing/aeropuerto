package castaing.miranda.gui;

import castaing.miranda.ui.Main;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.SECONDS;

public class ListaVuelos {

    private SimpleStringProperty numero;
    private SimpleStringProperty lineaAerea;
    private SimpleStringProperty origen;
    private SimpleStringProperty destino;
    private SimpleStringProperty sala;
    private SimpleStringProperty estado;
    private SimpleStringProperty cronometro;
    private Button btnEditar;
    private Button btnEliminar;

    public ListaVuelos(String numero, String lineaAerea, String origen, String destino, String estado) {
        this.numero = new SimpleStringProperty(numero);
        this.lineaAerea = new SimpleStringProperty(lineaAerea);
        this.origen = new SimpleStringProperty(origen);
        this.destino = new SimpleStringProperty(destino);
        this.estado = new SimpleStringProperty(estado);
        this.btnEditar = new Button("Editar");
        this.btnEliminar = new Button("Eliminar");
        this.btnEditar.setStyle("-fx-border-radius: 100px; -fx-background-color: transparent; -fx-text-fill: white; -fx-border-color: white; " +
                "-fx-border-width: 1px; -fx-cursor: hand;");
        this.btnEditar.setId(numero);
        this.btnEditar.setOnMouseClicked(event -> {
            try {
                ModificarVuelo.numeroVuelo = this.btnEditar.getId();
                modificarVuelo(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        this.btnEliminar.setStyle("-fx-border-radius: 100px; -fx-background-color: transparent; -fx-text-fill: white; -fx-border-color: white; " +
                "-fx-border-width: 1px; -fx-cursor: hand;");
        this.btnEliminar.setId(numero);
        this.btnEliminar.setOnMouseClicked(event -> {
            try {
                Main.eliminarVuelo(this.btnEliminar.getId());
                listarVuelos(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public ListaVuelos(String lineaAerea, String numero, String origen, String destino, String sala, String estado,
                       String horaLlegada, String fechaLlegada, char tipo) throws ParseException {
        this.lineaAerea = new SimpleStringProperty(lineaAerea);
        this.numero = new SimpleStringProperty(numero);
        this.origen = new SimpleStringProperty(origen);
        this.destino = new SimpleStringProperty(destino);
        this.sala = new SimpleStringProperty(sala);
        this.estado = new SimpleStringProperty(estado);
        this.cronometro = new SimpleStringProperty("Cargando...");

        String fechaHoraLlegada = fechaLlegada + " " + horaLlegada;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date hoy = new Date();
        Date llegada = format.parse(fechaHoraLlegada);
        long diff = llegada.getTime() - hoy.getTime();

        long hours = TimeUnit.MILLISECONDS.toHours(diff);
        if (hours <= 24 && !estado.equals("Cancelado")) {
            long remainingMinutesInMillis = diff - TimeUnit.HOURS.toMillis(hours);
            long minutes = TimeUnit.MILLISECONDS.toMinutes(remainingMinutesInMillis);
            long remainingSecondsInMillis = remainingMinutesInMillis - TimeUnit.MINUTES.toMillis(minutes);
            long seconds = TimeUnit.MILLISECONDS.toSeconds(remainingSecondsInMillis);

            final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
            final Runnable refresh = new Runnable() {
                String horas = Long.toString(hours);
                String minutos = Long.toString(minutes);
                String segundos = Long.toString(seconds);

                public void run() {
                    segundos = Long.toString(Long.parseLong(segundos) - 1);
                    if (segundos.equals("-1")) {
                        minutos = Long.toString(Long.parseLong(minutos) - 1);
                        segundos = "59";
                    }
                    if (minutos.equals("-1")) {
                        horas = Long.toString(Long.parseLong(horas) - 1);
                        minutos = "59";
                    }
                    if (Long.parseLong(segundos) < 10) {
                        segundos = "0" + segundos;
                    }
                    if (Long.parseLong(minutos) < 10 && minutos.length() == 1) {
                        minutos = "0" + minutos;
                    }
                    if (Long.parseLong(horas) < 10 && horas.length() == 1) {
                        horas = "0" + horas;
                    }
                    setCronometro(horas + ":" + minutos + ":" + segundos);
                    if (horas.equals("00") && minutos.equals("00") && segundos.equals("00")) {
                        if (tipo == 'S') {
                            setCronometro("Despegó");
                        } else {
                            setCronometro("Arribó");
                        }
                        scheduler.shutdown();
                    }
                }
            };
            scheduler.scheduleAtFixedRate(refresh, 2, 1, SECONDS);
        } else if (!estado.equals("Cancelado")) {
            setCronometro("En espera");
        } else {
            setCronometro("No aplica");
        }
    }

    @FXML
    void modificarVuelo(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ModificarVuelo.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarVuelos(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarVuelos.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    public String getNumero() {
        return numero.get();
    }

    public SimpleStringProperty numeroProperty() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero.set(numero);
    }

    public String getLineaAerea() {
        return lineaAerea.get();
    }

    public SimpleStringProperty lineaAereaProperty() {
        return lineaAerea;
    }

    public void setLineaAerea(String lineaAerea) {
        this.lineaAerea.set(lineaAerea);
    }

    public String getOrigen() {
        return origen.get();
    }

    public SimpleStringProperty origenProperty() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen.set(origen);
    }

    public String getDestino() {
        return destino.get();
    }

    public SimpleStringProperty destinoProperty() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino.set(destino);
    }

    public String getSala() {
        return sala.get();
    }

    public SimpleStringProperty salaProperty() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala.set(sala);
    }

    public String getEstado() {
        return estado.get();
    }

    public SimpleStringProperty estadoProperty() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado.set(estado);
    }

    public String getCronometro() {
        return cronometro.get();
    }

    public SimpleStringProperty cronometroProperty() {
        return cronometro;
    }

    public void setCronometro(String cronometro) {
        this.cronometro.set(cronometro);
    }

    public Button getBtnEditar() {
        return btnEditar;
    }

    public void setBtnEditar(Button btnEditar) {
        this.btnEditar = btnEditar;
    }

    public Button getBtnEliminar() {
        return btnEliminar;
    }

    public void setBtnEliminar(Button btnEliminar) {
        this.btnEliminar = btnEliminar;
    }

}
