package castaing.miranda.gui;

import castaing.miranda.ui.Main;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ListaPaises {

    private SimpleStringProperty codigo;
    private SimpleStringProperty nombre;
    private SimpleStringProperty abreviatura;
    private Button btnEditar;
    private Button btnEliminar;

    public ListaPaises(String codigo, String nombre, String abreviatura) {
        this.codigo = new SimpleStringProperty(codigo);
        this.nombre = new SimpleStringProperty(nombre);
        this.abreviatura = new SimpleStringProperty(abreviatura);
        this.btnEditar = new Button("Editar");
        this.btnEliminar = new Button("Eliminar");
        this.btnEditar.setStyle("-fx-border-radius: 100px; -fx-background-color: transparent; -fx-text-fill: white; -fx-border-color: white; " +
                "-fx-border-width: 1px; -fx-cursor: hand;");
        this.btnEditar.setId(codigo);
        this.btnEditar.setOnMouseClicked(event -> {
            try {
                ModificarPais.codigoPais = this.btnEditar.getId();
                modificarPais(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        this.btnEliminar.setStyle("-fx-border-radius: 100px; -fx-background-color: transparent; -fx-text-fill: white; -fx-border-color: white; " +
                "-fx-border-width: 1px; -fx-cursor: hand;");
        this.btnEliminar.setId(codigo);
        this.btnEliminar.setOnMouseClicked(event -> {
            try {
                Main.eliminarPais(this.btnEliminar.getId());
                listarPaises(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @FXML
    void modificarPais(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ModificarPais.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarPaises(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarPaises.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    public String getCodigo() {
        return codigo.get();
    }

    public SimpleStringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    public String getNombre() {
        return nombre.get();
    }

    public SimpleStringProperty nombreProperty() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public String getAbreviatura() {
        return abreviatura.get();
    }

    public SimpleStringProperty abreviaturaProperty() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura.set(abreviatura);
    }

    public Button getBtnEditar() {
        return btnEditar;
    }

    public void setBtnEditar(Button btnEditar) {
        this.btnEditar = btnEditar;
    }

    public Button getBtnEliminar() {
        return btnEliminar;
    }

    public void setBtnEliminar(Button btnEliminar) {
        this.btnEliminar = btnEliminar;
    }

}
