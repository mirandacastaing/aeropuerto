package castaing.miranda.gui;

import castaing.miranda.ui.Main;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.net.URL;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

public class Programados implements Initializable {

    @FXML
    private TableView vuelos;
    public TableColumn<ListaVuelos, String> lineaAerea;
    public TableColumn<ListaVuelos, String> numero;
    public TableColumn<ListaVuelos, String> origen;
    public TableColumn<ListaVuelos, String> destino;
    public TableColumn<ListaVuelos, String> sala;
    public TableColumn<ListaVuelos, String> estado;
    public TableColumn<ListaVuelos, String> cronometro;

    @FXML
    private TextField filtro;

    @FXML
    private ChoiceBox<String> paises;

    @FXML
    private Label usuario;

    double x = 0, y = 0;

    @FXML
    void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    void listarProgramados(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("Programados.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarRealizados(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("Salidas.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void modificarUsuario(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("modificarUsuario.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void cerrarSesion(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("IniciarSesion.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        usuario.setText(Main.enviarNombreCompletoPersonaIngresada());

        String[][] infoPaises = Main.enviarListaPaises();
        listaPaises.add("Seleccione el país");
        for (int i = 0; i < infoPaises.length; i++) {
            listaPaises.add(infoPaises[i][1]);
        }
        paises.setItems(listaPaises);
        paises.getSelectionModel().selectFirst();

        lineaAerea.setCellValueFactory(cellData -> cellData.getValue().lineaAereaProperty());
        numero.setCellValueFactory(cellData -> cellData.getValue().numeroProperty());
        origen.setCellValueFactory(cellData -> cellData.getValue().origenProperty());
        destino.setCellValueFactory(cellData -> cellData.getValue().destinoProperty());
        sala.setCellValueFactory(cellData -> cellData.getValue().salaProperty());
        estado.setCellValueFactory(cellData -> cellData.getValue().estadoProperty());
        cronometro.setCellValueFactory(cellData -> cellData.getValue().cronometroProperty());

        String[][] infoVuelos = Main.enviarListaVuelosMiembroTripulacion();
        for (int i = 0; i < infoVuelos.length; i++) {
            LocalDate fechaLlegada = LocalDate.parse(infoVuelos[i][13] + " " + infoVuelos[i][5], DateTimeFormatter.ofPattern("yyyy-MM-dd H:m"));
            LocalDate fechaSalida = LocalDate.parse(infoVuelos[i][14] + " " + infoVuelos[i][3], DateTimeFormatter.ofPattern("yyyy-MM-dd H:m"));
            String status = infoVuelos[i][6];
            if (((fechaLlegada.isAfter(LocalDate.now()) || (!status.equals("L") && !status.equals("C"))) &&
                    infoVuelos[i][7].equals("L")) || ((fechaSalida.isAfter(LocalDate.now()) ||
                    (!status.equals("L") && !status.equals("C"))) && infoVuelos[i][7].equals("S"))) {
                switch (status) {
                    case "R":
                        status = "Registrado";
                        break;
                    case "A":
                        status = "Atrasado";
                        break;
                    case "T":
                        status = "A tiempo";
                        break;
                    case "L":
                        status = "Llegó";
                        break;
                    case "C":
                        status = "Cancelado";
                        break;
                    default:
                        status = "Indefinido";
                        break;
                }
                try {
                    if (infoVuelos[i][7].equals("L")) {
                        listaVuelos.add(new ListaVuelos(infoVuelos[i][11], infoVuelos[i][0], infoVuelos[i][1],
                                "Aeropuerto Internacional de Orotina", infoVuelos[i][2], status, infoVuelos[i][5],
                                infoVuelos[i][13], 'L'));
                    } else {
                        listaVuelos.add(new ListaVuelos(infoVuelos[i][11], infoVuelos[i][0],
                                "Aeropuerto Internacional de Orotina", infoVuelos[i][4],
                                infoVuelos[i][2], status, infoVuelos[i][3], infoVuelos[i][14], 'S'));
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        FilteredList<ListaVuelos> filteredData = new FilteredList<>(listaVuelos, p -> true);

        filtro.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(vuelo -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if (vuelo.getLineaAerea().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (vuelo.getNumero().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (vuelo.getOrigen().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (vuelo.getDestino().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (vuelo.getSala().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (vuelo.getEstado().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (vuelo.getCronometro().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                return false;
            });
        });

        paises.getSelectionModel()
                .selectedItemProperty()
                .addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    listaVuelos.clear();
                    if (paises.getValue().equals("Seleccione el país")) {
                        for (int i = 0; i < infoVuelos.length; i++) {
                            LocalDate fechaLlegada = LocalDate.parse(infoVuelos[i][13] + " " + infoVuelos[i][5], DateTimeFormatter.ofPattern("yyyy-MM-dd H:m"));
                            LocalDate fechaSalida = LocalDate.parse(infoVuelos[i][14] + " " + infoVuelos[i][3], DateTimeFormatter.ofPattern("yyyy-MM-dd H:m"));
                            String status = infoVuelos[i][6];
                            if (((fechaLlegada.isAfter(LocalDate.now()) || (!status.equals("L") && !status.equals("C"))) &&
                                    infoVuelos[i][7].equals("L")) || ((fechaSalida.isAfter(LocalDate.now()) ||
                                    (!status.equals("L") && !status.equals("C"))) && infoVuelos[i][7].equals("S"))) {
                                switch (status) {
                                    case "R":
                                        status = "Registrado";
                                        break;
                                    case "A":
                                        status = "Atrasado";
                                        break;
                                    case "T":
                                        status = "A tiempo";
                                        break;
                                    case "L":
                                        status = "Llegó";
                                        break;
                                    case "C":
                                        status = "Cancelado";
                                        break;
                                    default:
                                        status = "Indefinido";
                                        break;
                                }
                                try {
                                    if (infoVuelos[i][7].equals("L")) {
                                        listaVuelos.add(new ListaVuelos(infoVuelos[i][11], infoVuelos[i][0], infoVuelos[i][1],
                                                "Aeropuerto Internacional de Orotina", infoVuelos[i][2], status, infoVuelos[i][5],
                                                infoVuelos[i][13], 'L'));
                                    } else {
                                        listaVuelos.add(new ListaVuelos(infoVuelos[i][11], infoVuelos[i][0],
                                                "Aeropuerto Internacional de Orotina", infoVuelos[i][4],
                                                infoVuelos[i][2], status, infoVuelos[i][3], infoVuelos[i][14], 'S'));
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } else {
                        for (int i = 0; i < infoVuelos.length; i++) {
                            if (infoVuelos[i][12].equals(paises.getValue()) || infoVuelos[i][15].equals(paises.getValue())) {
                                LocalDate fechaLlegada = LocalDate.parse(infoVuelos[i][13] + " " + infoVuelos[i][5], DateTimeFormatter.ofPattern("yyyy-MM-dd H:m"));
                                LocalDate fechaSalida = LocalDate.parse(infoVuelos[i][14] + " " + infoVuelos[i][3], DateTimeFormatter.ofPattern("yyyy-MM-dd H:m"));
                                String status = infoVuelos[i][6];
                                if (((fechaLlegada.isAfter(LocalDate.now()) || (!status.equals("L") && !status.equals("C"))) &&
                                        infoVuelos[i][7].equals("L")) || ((fechaSalida.isAfter(LocalDate.now()) ||
                                        (!status.equals("L") && !status.equals("C"))) && infoVuelos[i][7].equals("S"))) {
                                    switch (status) {
                                        case "R":
                                            status = "Registrado";
                                            break;
                                        case "A":
                                            status = "Atrasado";
                                            break;
                                        case "T":
                                            status = "A tiempo";
                                            break;
                                        case "L":
                                            status = "Llegó";
                                            break;
                                        case "C":
                                            status = "Cancelado";
                                            break;
                                        default:
                                            status = "Indefinido";
                                            break;
                                    }
                                    try {
                                        if (infoVuelos[i][7].equals("L")) {
                                            listaVuelos.add(new ListaVuelos(infoVuelos[i][11], infoVuelos[i][0], infoVuelos[i][1],
                                                    "Aeropuerto Internacional de Orotina", infoVuelos[i][2], status, infoVuelos[i][5],
                                                    infoVuelos[i][13], 'L'));
                                        } else {
                                            listaVuelos.add(new ListaVuelos(infoVuelos[i][11], infoVuelos[i][0],
                                                    "Aeropuerto Internacional de Orotina", infoVuelos[i][4],
                                                    infoVuelos[i][2], status, infoVuelos[i][3], infoVuelos[i][14], 'S'));
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                    filtro.setText(filtro.getText());
                });

        SortedList<ListaVuelos> sortedData = new SortedList<>(filteredData);

        sortedData.comparatorProperty().bind(vuelos.comparatorProperty());

        vuelos.setItems(sortedData);

    }

    private ObservableList<ListaVuelos> listaVuelos = FXCollections.observableArrayList();
    private ObservableList<String> listaPaises = FXCollections.observableArrayList();

}
