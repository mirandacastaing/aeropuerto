package castaing.miranda.gui;

import castaing.miranda.ui.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import java.net.URL;
import java.util.ResourceBundle;

public class ListarLineasAereas implements Initializable {

    @FXML
    private TableView lineasAereas;
    public TableColumn<ListaLineasAereas, String> logo;
    public TableColumn<ListaLineasAereas, String> cedula;
    public TableColumn<ListaLineasAereas, String> nombreComercial;
    public TableColumn<ListaLineasAereas, String> pais;
    public TableColumn<ListaLineasAereas, String> nombreEmpresa;
    public TableColumn editar;
    public TableColumn eliminar;

    @FXML
    private TextField filtro;

    @FXML
    private Label usuario;

    double x = 0, y = 0;

    @FXML
    void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    void listarVuelos(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarVuelos.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarAeronaves(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarAeronaves.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarLineasAereas(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarLineasAereas.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarPaises(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarPaises.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarSalas(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarSalas.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarUbicaciones(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarUbicaciones.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarTripulaciones(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarTripulaciones.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void definirImpuesto(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("DefinirImpuesto.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void modificarUsuario(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("modificarUsuario.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void cerrarSesion(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("IniciarSesion.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void registrarLineaAerea(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("RegistrarLineaAerea.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        usuario.setText(Main.enviarNombreCompletoPersonaIngresada());

        logo.setCellValueFactory(cellData -> cellData.getValue().logoProperty());
        cedula.setCellValueFactory(cellData -> cellData.getValue().cedulaProperty());
        nombreComercial.setCellValueFactory(cellData -> cellData.getValue().nombreComercialProperty());
        pais.setCellValueFactory(cellData -> cellData.getValue().paisProperty());
        nombreEmpresa.setCellValueFactory(cellData -> cellData.getValue().nombreEmpresaProperty());
        editar.setCellValueFactory(new PropertyValueFactory<ListaLineasAereas, String>("btnEditar"));
        eliminar.setCellValueFactory(new PropertyValueFactory<ListaLineasAereas, String>("btnEliminar"));

        String[][] infoLineasAereas = Main.enviarListaLineasAereas();
        for (int i = 0; i < infoLineasAereas.length; i++) {
            listaLineasAereas.add(new ListaLineasAereas(infoLineasAereas[i][0], infoLineasAereas[i][1],
                    infoLineasAereas[i][2], infoLineasAereas[i][3], infoLineasAereas[i][4]));
        }

        FilteredList<ListaLineasAereas> filteredData = new FilteredList<>(listaLineasAereas, p -> true);

        filtro.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(lineaAerea -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if (lineaAerea.getLogo().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (lineaAerea.getCedula().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (lineaAerea.getNombreComercial().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (lineaAerea.getPais().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (lineaAerea.getNombreEmpresa().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                return false;
            });
        });

        SortedList<ListaLineasAereas> sortedData = new SortedList<>(filteredData);

        sortedData.comparatorProperty().bind(lineasAereas.comparatorProperty());

        lineasAereas.setItems(sortedData);
    }

    private ObservableList<ListaLineasAereas> listaLineasAereas = FXCollections.observableArrayList();

}
