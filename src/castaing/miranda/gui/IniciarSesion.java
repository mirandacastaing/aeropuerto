package castaing.miranda.gui;

import castaing.miranda.ui.Main;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.ResourceBundle;

public class IniciarSesion implements Initializable {

    @FXML
    private TextField correo, contrasenna;

    double x = 0, y = 0;

    @FXML
    void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    void enviarCredenciales(MouseEvent event) throws IOException {
        if (validar()) {
            String fxml = Main.recibirCredenciales(correo.getText(), contrasenna.getText());
            if (!fxml.equals(" ")) {
                if (!fxml.equals("administradorSinAeropuerto")) {
                    ((Node) (event.getSource())).getScene().getWindow().hide();
                    Stage window = new Stage();
                    Scene scene = new Scene(FXMLLoader.load(getClass().getResource(fxml)));
                    scene.setFill(Color.TRANSPARENT);
                    window.setScene(scene);
                    window.initStyle(StageStyle.TRANSPARENT);
                    window.show();
                } else {
                    RegistrarAeropuerto.correoAdministrador = correo.getText();
                    ((Node) (event.getSource())).getScene().getWindow().hide();
                    Stage window = new Stage();
                    Scene scene = new Scene(FXMLLoader.load(getClass().getResource("RegistrarAeropuertoIniciarSesion.fxml")));
                    scene.setFill(Color.TRANSPARENT);
                    window.setScene(scene);
                    window.initStyle(StageStyle.TRANSPARENT);
                    window.show();
                }
            }
        }
    }

        @FXML
        void crearCuenta (MouseEvent event) throws Exception {
            ((Node) (event.getSource())).getScene().getWindow().hide();
            Stage window = new Stage();
            Scene scene = new Scene(FXMLLoader.load(getClass().getResource("RegistrarUsuario.fxml")));
            scene.setFill(Color.TRANSPARENT);
            window.setScene(scene);
            window.initStyle(StageStyle.TRANSPARENT);
            window.show();
        }

        @Override
        public void initialize (URL location, ResourceBundle resources){
        }

        public boolean validar() {
            boolean validacionCorrecta = true;
            if (correo.getText().equals("")) {
                correo.getStyleClass().add("error");
                validacionCorrecta = false;
            } else {
                correo.getStyleClass().removeAll(Collections.singleton("error"));
            }
            if (contrasenna.getText().equals("")) {
                contrasenna.getStyleClass().add("error");
                validacionCorrecta = false;
            } else {
                contrasenna.getStyleClass().removeAll(Collections.singleton("error"));
            }
            return validacionCorrecta;
        }

    }
