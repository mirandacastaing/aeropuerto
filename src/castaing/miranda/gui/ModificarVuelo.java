package castaing.miranda.gui;

import castaing.miranda.ui.Main;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

public class ModificarVuelo implements Initializable {

    static String numeroVuelo;

    @FXML
    private TextField numero, horaSalida, horaLlegada, precio;

    @FXML
    private ChoiceBox<String> aeropuertosOrigen, aeropuertosDestino, tipos, salas, aeronaves, tripulaciones,
            lineasAereas, estados;

    @FXML
    private DatePicker fechaLlegada, fechaSalida;

    @FXML
    private Label usuario;

    double x = 0, y = 0;

    @FXML
    void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    void listarVuelos(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarVuelos.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarAeronaves(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarAeronaves.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarLineasAereas(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarLineasAereas.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarPaises(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarPaises.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarSalas(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarSalas.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarUbicaciones(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarUbicaciones.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarTripulaciones(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarTripulaciones.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void definirImpuesto(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("DefinirImpuesto.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void modificarUsuario(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("modificarUsuario.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void cerrarSesion(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("IniciarSesion.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void enviarInfoVuelo(MouseEvent event) throws Exception {
        String status = estados.getValue();
        switch (status) {
            case "Registrado":
                status = "R";
                break;
            case "Atrasado":
                status = "A";
                break;
            case "A tiempo":
                status = "T";
                break;
            case "Llegó":
                status = "L";
                break;
            case "Cancelado":
                status = "C";
                break;
            default:
                status = "Indefinido";
                break;
        }
        Main.actualizarInfoVuelo(lineasAereas.getValue(), aeropuertosOrigen.getValue(), horaLlegada.getText(),
                horaSalida.getText(), aeropuertosDestino.getValue(), tipos.getValue().charAt(0), aeronaves.getValue(),
                salas.getValue(), tripulaciones.getValue(), numero.getText(), status.charAt(0),
                fechaLlegada.getValue().toString(), fechaSalida.getValue().toString(),
                Double.parseDouble(precio.getText()));
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarVuelos.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        usuario.setText(Main.enviarNombreCompletoPersonaIngresada());

        String[] info = Main.enviarInfoVuelo(numeroVuelo);
        String[][] infoLineasAereas = Main.enviarListaLineasAereas();
        String[][] infoSalas = Main.enviarListaSalas();
        String[][] infoAeronaves = Main.enviarListaAeronaves();
        String[][] infoTripulaciones = Main.enviarListaTripulaciones();
        String[][] infoAeropuertos = Main.enviarListaAeropuertos();

        for (int i = 0; i < infoLineasAereas.length; i++) {
            listaLineasAereas.add(infoLineasAereas[i][2]);
        }
        lineasAereas.setItems(listaLineasAereas);

        for (int i = 0; i < infoAeropuertos.length; i++) {
            listaAeropuertosOrigen.add(infoAeropuertos[i][1]);
        }
        aeropuertosOrigen.setItems(listaAeropuertosOrigen);

        for (int i = 0; i < infoAeropuertos.length; i++) {
            if (!infoAeropuertos[i][1].equals(aeropuertosOrigen.getValue())) {
                listaAeropuertosDestino.add(infoAeropuertos[i][1]);
            }
        }
        aeropuertosDestino.setItems(listaAeropuertosDestino);

        listaTipos.add("Llegada");
        listaTipos.add("Salida");
        tipos.setItems(listaTipos);

        for (int i = 0; i < infoAeronaves.length; i++) {
            if (infoAeronaves[i][0].equals(lineasAereas.getValue())) {
                listaAeronaves.add(infoAeronaves[i][1]);
            }
        }
        aeronaves.setItems(listaAeronaves);

        for (int i = 0; i < infoSalas.length; i++) {
            listaSalas.add(infoSalas[i][1]);
        }
        salas.setItems(listaSalas);

        for (int i = 0; i < infoTripulaciones.length; i++) {
            if (infoTripulaciones[i][2].equals(lineasAereas.getValue())) {
                listaTripulaciones.add(infoTripulaciones[i][1]);
            }
        }
        tripulaciones.setItems(listaTripulaciones);

        switch (info[10]) {
            case "R":
                listaEstados.add("Registrado");
                listaEstados.add("Atrasado");
                listaEstados.add("Cancelado");
                break;
            case "A":
                listaEstados.add("Atrasado");
                listaEstados.add("Cancelado");
                break;
            case "T":
                listaEstados.add("A tiempo");
                listaEstados.add("Atrasado");
                listaEstados.add("Cancelado");
                break;
            case "L":
                listaEstados.add("Llegó");
                break;
            case "C":
                listaEstados.add("Cancelado");
                break;
            default:
                break;
        }
        estados.setItems(listaEstados);

        aeropuertosOrigen.getSelectionModel()
                .selectedItemProperty()
                .addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    listaAeropuertosDestino.clear();
                    for (int i = 0; i < infoAeropuertos.length; i++) {
                        if (!infoAeropuertos[i][0].equals(aeropuertosOrigen.getValue())) {
                            listaAeropuertosDestino.add(infoAeropuertos[i][1]);
                        }
                    }
                    aeropuertosDestino.setItems(listaAeropuertosDestino);
                    aeropuertosDestino.getSelectionModel().selectFirst();});

        lineasAereas.getSelectionModel()
                .selectedItemProperty()
                .addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    listaAeronaves.clear();
                    listaTripulaciones.clear();
                    for (int i = 0; i < infoAeronaves.length; i++) {
                        if (infoAeronaves[i][0].equals(lineasAereas.getValue())) {
                            listaAeronaves.add(infoAeronaves[i][1]);
                        }
                    }
                    aeronaves.setItems(listaAeronaves);
                    aeronaves.getSelectionModel().selectFirst();
                    for (int i = 0; i < infoTripulaciones.length; i++) {
                        if (infoTripulaciones[i][2].equals(lineasAereas.getValue())) {
                            listaTripulaciones.add(infoTripulaciones[i][1]);
                        }
                    }
                    tripulaciones.setItems(listaTripulaciones);
                    tripulaciones.getSelectionModel().selectFirst();});

        numero.setText(info[0]);
        aeropuertosOrigen.getSelectionModel().select(info[1]);
        fechaLlegada.setValue(LocalDate.parse(info[12], DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        horaSalida.setText(info[2]);
        aeropuertosDestino.getSelectionModel().select(info[3]);
        fechaSalida.setValue(LocalDate.parse(info[11], DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        horaLlegada.setText(info[4]);
        precio.setText(info[13]);
        if (info[5].equals("L")) {
            tipos.getSelectionModel().select("Llegada");
        } else {
            tipos.getSelectionModel().select("Salida");
        }
        salas.getSelectionModel().select(info[6]);
        aeronaves.getSelectionModel().select(info[7]);
        tripulaciones.getSelectionModel().select(info[8]);
        lineasAereas.getSelectionModel().select(info[9]);
        String status = info[10];
        switch (status) {
            case "R":
                status = "Registrado";
                break;
            case "A":
                status = "Atrasado";
                break;
            case "T":
                status = "A tiempo";
                break;
            case "L":
                status = "Llegó";
                break;
            case "C":
                status = "Cancelado";
                break;
            default:
                status = "Indefinido";
                break;
        }
        estados.getSelectionModel().select(status);

        estados.getSelectionModel()
                .selectedItemProperty()
                .addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    if (estados.getValue().equals("Atrasado") && oldValue.equals("A tiempo")) {
                        if (tipos.getValue().equals("Llegada")) {
                            horaLlegada.clear();
                        } else {
                            horaSalida.clear();
                        }
                    } else if (estados.getValue().equals("Atrasado") && oldValue.equals("Registrado")) {
                        horaSalida.clear();
                        horaLlegada.clear();
                    }
                });
    }

    private ObservableList<String> listaLineasAereas = FXCollections.observableArrayList();
    private ObservableList<String> listaAeropuertosOrigen = FXCollections.observableArrayList();
    private ObservableList<String> listaAeropuertosDestino = FXCollections.observableArrayList();
    private ObservableList<String> listaTipos = FXCollections.observableArrayList();
    private ObservableList<String> listaAeronaves = FXCollections.observableArrayList();
    private ObservableList<String> listaSalas = FXCollections.observableArrayList();
    private ObservableList<String> listaTripulaciones = FXCollections.observableArrayList();
    private ObservableList<String> listaEstados = FXCollections.observableArrayList();

}
