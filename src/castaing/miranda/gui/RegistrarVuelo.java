package castaing.miranda.gui;

import castaing.miranda.ui.Main;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import java.net.URL;
import java.util.ResourceBundle;

public class RegistrarVuelo implements Initializable {

    @FXML
    private TextField numero, horaSalida, horaLlegada, precio;

    @FXML
    private ChoiceBox<String> aeropuertosOrigen, aeropuertosDestino, tipos, salas, aeronaves, tripulaciones,
            lineasAereas;

    @FXML
    private DatePicker fechaLlegada, fechaSalida;

    @FXML
    private Label usuario;

    double x = 0, y = 0;

    @FXML
    void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    void listarVuelos(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarVuelos.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarAeronaves(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarAeronaves.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarLineasAereas(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarLineasAereas.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarPaises(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarPaises.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarSalas(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarSalas.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarUbicaciones(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarUbicaciones.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarTripulaciones(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarTripulaciones.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void definirImpuesto(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("DefinirImpuesto.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void modificarUsuario(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("modificarUsuario.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void cerrarSesion(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("IniciarSesion.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void enviarInfoVuelo(MouseEvent event) throws Exception {
        if (!Main.verificarVuelo(numero.getText())) {
            Main.recibirInfoVuelo(lineasAereas.getValue(), aeropuertosOrigen.getValue(), horaLlegada.getText(),
                    horaSalida.getText(), aeropuertosDestino.getValue(), tipos.getValue().charAt(0), aeronaves.getValue(),
                    salas.getValue(), tripulaciones.getValue(), numero.getText(), fechaLlegada.getValue().toString(),
                    fechaSalida.getValue().toString(), Double.parseDouble(precio.getText()));
            ((Node) (event.getSource())).getScene().getWindow().hide();
            Stage window = new Stage();
            Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarVuelos.fxml")));
            scene.setFill(Color.TRANSPARENT);
            window.setScene(scene);
            window.initStyle(StageStyle.TRANSPARENT);
            window.show();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        usuario.setText(Main.enviarNombreCompletoPersonaIngresada());

        String[][] infoLineasAereas = Main.enviarListaLineasAereas();
        String[][] infoSalas = Main.enviarListaSalas();
        String[][] infoAeronaves = Main.enviarListaAeronaves();
        String[][] infoTripulaciones = Main.enviarListaTripulaciones();
        String[][] infoAeropuertos = Main.enviarListaAeropuertos();

        for (int i = 0; i < infoLineasAereas.length; i++) {
            listaLineasAereas.add(infoLineasAereas[i][2]);
        }
        lineasAereas.setItems(listaLineasAereas);
        lineasAereas.getSelectionModel().selectFirst();

        for (int i = 0; i < infoAeropuertos.length; i++) {
            listaAeropuertosOrigen.add(infoAeropuertos[i][1]);
        }
        aeropuertosOrigen.setItems(listaAeropuertosOrigen);
        aeropuertosOrigen.getSelectionModel().selectFirst();

        for (int i = 0; i < infoAeropuertos.length; i++) {
            if (!infoAeropuertos[i][1].equals(aeropuertosOrigen.getValue())) {
                listaAeropuertosDestino.add(infoAeropuertos[i][1]);
            }
        }
        aeropuertosDestino.setItems(listaAeropuertosDestino);
        aeropuertosDestino.getSelectionModel().selectFirst();

        listaTipos.add("Llegada");
        listaTipos.add("Salida");
        tipos.setItems(listaTipos);
        tipos.getSelectionModel().selectFirst();

        for (int i = 0; i < infoAeronaves.length; i++) {
            if (infoAeronaves[i][0].equals(lineasAereas.getValue())) {
                listaAeronaves.add(infoAeronaves[i][1]);
            }
        }
        aeronaves.setItems(listaAeronaves);
        aeronaves.getSelectionModel().selectFirst();

        for (int i = 0; i < infoSalas.length; i++) {
            listaSalas.add(infoSalas[i][1]);
        }
        salas.setItems(listaSalas);
        salas.getSelectionModel().selectFirst();

        for (int i = 0; i < infoTripulaciones.length; i++) {
            if (infoTripulaciones[i][2].equals(lineasAereas.getValue())) {
                listaTripulaciones.add(infoTripulaciones[i][1]);
            }
        }
        tripulaciones.setItems(listaTripulaciones);
        tripulaciones.getSelectionModel().selectFirst();

        aeropuertosOrigen.getSelectionModel()
                .selectedItemProperty()
                .addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                        listaAeropuertosDestino.clear();
                        for (int i = 0; i < infoAeropuertos.length; i++) {
                            if (!infoAeropuertos[i][0].equals(aeropuertosOrigen.getValue())) {
                                listaAeropuertosDestino.add(infoAeropuertos[i][1]);
                            }
                        }
                        aeropuertosDestino.setItems(listaAeropuertosDestino);
                        aeropuertosDestino.getSelectionModel().selectFirst();});

        lineasAereas.getSelectionModel()
                .selectedItemProperty()
                .addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    listaAeronaves.clear();
                    listaTripulaciones.clear();
                    for (int i = 0; i < infoAeronaves.length; i++) {
                        if (infoAeronaves[i][0].equals(lineasAereas.getValue())) {
                            listaAeronaves.add(infoAeronaves[i][1]);
                        }
                    }
                    aeronaves.setItems(listaAeronaves);
                    aeronaves.getSelectionModel().selectFirst();
                    for (int i = 0; i < infoTripulaciones.length; i++) {
                        if (infoTripulaciones[i][2].equals(lineasAereas.getValue())) {
                            listaTripulaciones.add(infoTripulaciones[i][1]);
                        }
                    }
                    tripulaciones.setItems(listaTripulaciones);
                    tripulaciones.getSelectionModel().selectFirst();});
    }

    private ObservableList<String> listaLineasAereas = FXCollections.observableArrayList();
    private ObservableList<String> listaAeropuertosOrigen = FXCollections.observableArrayList();
    private ObservableList<String> listaAeropuertosDestino = FXCollections.observableArrayList();
    private ObservableList<String> listaTipos = FXCollections.observableArrayList();
    private ObservableList<String> listaAeronaves = FXCollections.observableArrayList();
    private ObservableList<String> listaSalas = FXCollections.observableArrayList();
    private ObservableList<String> listaTripulaciones = FXCollections.observableArrayList();

}
