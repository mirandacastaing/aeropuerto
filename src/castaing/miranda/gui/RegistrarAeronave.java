package castaing.miranda.gui;

import castaing.miranda.ui.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import java.net.URL;
import java.util.ResourceBundle;

public class RegistrarAeronave implements Initializable {

    @FXML
    private TextField placa, marca, modelo, capacidad;

    @FXML
    private ChoiceBox<String> lineasAereas;

    @FXML
    private Label usuario;

    double x = 0, y = 0;

    @FXML
    void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    void listarVuelos(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarVuelos.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarAeronaves(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarAeronaves.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarLineasAereas(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarLineasAereas.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarPaises(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarPaises.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarSalas(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarSalas.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarUbicaciones(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarUbicaciones.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarTripulaciones(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarTripulaciones.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void definirImpuesto(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("DefinirImpuesto.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void modificarUsuario(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("modificarUsuario.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void cerrarSesion(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("IniciarSesion.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void enviarInfoAeronave(MouseEvent event) throws Exception {
        if (!Main.verificarAeronave(placa.getText())) {
            Main.recibirInfoAeronave(lineasAereas.getValue(), placa.getText(), marca.getText(), modelo.getText(),
                    Integer.parseInt(capacidad.getText()));
            ((Node) (event.getSource())).getScene().getWindow().hide();
            Stage window = new Stage();
            Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarAeronaves.fxml")));
            scene.setFill(Color.TRANSPARENT);
            window.setScene(scene);
            window.initStyle(StageStyle.TRANSPARENT);
            window.show();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        usuario.setText(Main.enviarNombreCompletoPersonaIngresada());

        String[][] infoLineasAereas = Main.enviarListaLineasAereas();
        for (int i = 0; i < infoLineasAereas.length; i++) {
            listaLineasAereas.add(infoLineasAereas[i][2]);
        }
        lineasAereas.setItems(listaLineasAereas);
        lineasAereas.getSelectionModel().selectFirst();
    }

    private ObservableList<String> listaLineasAereas = FXCollections.observableArrayList();

}
