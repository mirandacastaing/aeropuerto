package castaing.miranda.gui;

import castaing.miranda.ui.Main;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ListaAeronaves {

    private SimpleStringProperty lineaAerea;
    private SimpleStringProperty placa;
    private SimpleStringProperty marca;
    private SimpleStringProperty modelo;
    private SimpleStringProperty capacidad;
    private Button btnEditar;
    private Button btnEliminar;

    public ListaAeronaves(String lineaAerea, String placa, String marca, String modelo, String capacidad) {
        this.lineaAerea = new SimpleStringProperty(lineaAerea);
        this.placa = new SimpleStringProperty(placa);
        this.marca = new SimpleStringProperty(marca);
        this.modelo = new SimpleStringProperty(modelo);
        this.capacidad = new SimpleStringProperty(capacidad);
        this.btnEditar = new Button("Editar");
        this.btnEliminar = new Button("Eliminar");
        this.btnEditar.setStyle("-fx-border-radius: 100px; -fx-background-color: transparent; -fx-text-fill: white; -fx-border-color: white; " +
                "-fx-border-width: 1px; -fx-cursor: hand;");
        this.btnEditar.setId(placa);
        this.btnEditar.setOnMouseClicked(event -> {
            try {
                ModificarAeronave.placaAeronave = this.btnEditar.getId();
                modificarAeronave(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        this.btnEliminar.setStyle("-fx-border-radius: 100px; -fx-background-color: transparent; -fx-text-fill: white; -fx-border-color: white; " +
                "-fx-border-width: 1px; -fx-cursor: hand;");
        this.btnEliminar.setId(placa);
        this.btnEliminar.setOnMouseClicked(event -> {
            try {
                Main.eliminarAeronave(this.btnEliminar.getId());
                listarAeronaves(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @FXML
    void modificarAeronave(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ModificarAeronave.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarAeronaves(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarAeronaves.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    public String getLineaAerea() {
        return lineaAerea.get();
    }

    public SimpleStringProperty lineaAereaProperty() {
        return lineaAerea;
    }

    public void setLineaAerea(String lineaAerea) {
        this.lineaAerea.set(lineaAerea);
    }

    public String getPlaca() {
        return placa.get();
    }

    public SimpleStringProperty placaProperty() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa.set(placa);
    }

    public String getMarca() {
        return marca.get();
    }

    public SimpleStringProperty marcaProperty() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca.set(marca);
    }

    public String getModelo() {
        return modelo.get();
    }

    public SimpleStringProperty modeloProperty() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo.set(modelo);
    }

    public String getCapacidad() {
        return capacidad.get();
    }

    public SimpleStringProperty capacidadProperty() {
        return capacidad;
    }

    public void setCapacidad(String capacidad) {
        this.capacidad.set(capacidad);
    }

    public Button getBtnEditar() {
        return btnEditar;
    }

    public void setBtnEditar(Button btnEditar) {
        this.btnEditar = btnEditar;
    }

    public Button getBtnEliminar() {
        return btnEliminar;
    }

    public void setBtnEliminar(Button btnEliminar) {
        this.btnEliminar = btnEliminar;
    }

}
