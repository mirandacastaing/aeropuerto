package castaing.miranda.gui;

import castaing.miranda.ui.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import java.net.URL;
import java.util.ResourceBundle;

public class ListarVuelos implements Initializable {

    @FXML
    private TableView vuelos;
    public TableColumn<ListaVuelos, String> numero;
    public TableColumn<ListaVuelos, String> lineaAerea;
    public TableColumn<ListaVuelos, String> origen;
    public TableColumn<ListaVuelos, String> destino;
    public TableColumn<ListaVuelos, String> estado;
    public TableColumn editar;
    public TableColumn eliminar;

    @FXML
    private TextField filtro;

    @FXML
    private Label usuario;

    double x = 0, y = 0;

    @FXML
    void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    void listarVuelos(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarVuelos.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarAeronaves(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarAeronaves.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarLineasAereas(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarLineasAereas.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarPaises(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarPaises.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarSalas(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarSalas.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarUbicaciones(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarUbicaciones.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarTripulaciones(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarTripulaciones.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void definirImpuesto(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("DefinirImpuesto.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void modificarUsuario(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("modificarUsuario.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void cerrarSesion(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("IniciarSesion.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void registrarVuelo(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("RegistrarVuelo.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        usuario.setText(Main.enviarNombreCompletoPersonaIngresada());

        numero.setCellValueFactory(cellData -> cellData.getValue().numeroProperty());
        lineaAerea.setCellValueFactory(cellData -> cellData.getValue().lineaAereaProperty());
        origen.setCellValueFactory(cellData -> cellData.getValue().origenProperty());
        destino.setCellValueFactory(cellData -> cellData.getValue().destinoProperty());
        estado.setCellValueFactory(cellData -> cellData.getValue().estadoProperty());
        editar.setCellValueFactory(new PropertyValueFactory<ListaVuelos, String>("btnEditar"));
        eliminar.setCellValueFactory(new PropertyValueFactory<ListaVuelos, String>("btnEliminar"));

        String[][] infoVuelos = Main.enviarListaVuelos();
        for (int i = 0; i < infoVuelos.length; i++) {
            String status = infoVuelos[i][6];
            switch (status) {
                case "R":
                    status = "Registrado";
                    break;
                case "A":
                    status = "Atrasado";
                    break;
                case "T":
                    status = "A tiempo";
                    break;
                case "L":
                    status = "Llegó";
                    break;
                case "C":
                    status = "Cancelado";
                    break;
                default:
                    status = "Indefinido";
                    break;
            }
            listaVuelos.add(new ListaVuelos(infoVuelos[i][0], infoVuelos[i][11], infoVuelos[i][1],
                    infoVuelos[i][4], status));
        }

        FilteredList<ListaVuelos> filteredData = new FilteredList<>(listaVuelos, p -> true);

        filtro.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(vuelo -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if (vuelo.getNumero().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (vuelo.getLineaAerea().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (vuelo.getOrigen().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (vuelo.getDestino().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (vuelo.getEstado().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                return false;
            });
        });

        SortedList<ListaVuelos> sortedData = new SortedList<>(filteredData);

        sortedData.comparatorProperty().bind(vuelos.comparatorProperty());

        vuelos.setItems(sortedData);
    }

    private ObservableList<ListaVuelos> listaVuelos = FXCollections.observableArrayList();

}
