package castaing.miranda.gui;

import castaing.miranda.ui.Main;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ListaUbicaciones {

    private SimpleStringProperty piso;
    private SimpleStringProperty descripcion;
    private Button btnEditar;
    private Button btnEliminar;

    public ListaUbicaciones(String piso, String descripcion) {
        this.piso = new SimpleStringProperty(piso);
        this.descripcion = new SimpleStringProperty(descripcion);
        this.btnEditar = new Button("Editar");
        this.btnEliminar = new Button("Eliminar");
        this.btnEditar.setStyle("-fx-border-radius: 100px; -fx-background-color: transparent; -fx-text-fill: white; -fx-border-color: white; " +
                "-fx-border-width: 1px; -fx-cursor: hand;");
        this.btnEditar.setId(piso);
        this.btnEditar.setOnMouseClicked(event -> {
            try {
                ModificarUbicacion.piso = this.btnEditar.getId().charAt(0);
                modificarUbicacion(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        this.btnEliminar.setStyle("-fx-border-radius: 100px; -fx-background-color: transparent; -fx-text-fill: white; -fx-border-color: white; " +
                "-fx-border-width: 1px; -fx-cursor: hand;");
        this.btnEliminar.setId(piso);
        this.btnEliminar.setOnMouseClicked(event -> {
            try {
                Main.eliminarUbicacion(this.btnEliminar.getId().charAt(0));
                listarUbicaciones(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @FXML
    void modificarUbicacion(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ModificarUbicacion.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarUbicaciones(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarUbicaciones.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    public String getPiso() {
        return piso.get();
    }

    public SimpleStringProperty pisoProperty() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso.set(piso);
    }

    public String getDescripcion() {
        return descripcion.get();
    }

    public SimpleStringProperty descripcionProperty() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion.set(descripcion);
    }

    public Button getBtnEditar() {
        return btnEditar;
    }

    public void setBtnEditar(Button btnEditar) {
        this.btnEditar = btnEditar;
    }

    public Button getBtnEliminar() {
        return btnEliminar;
    }

    public void setBtnEliminar(Button btnEliminar) {
        this.btnEliminar = btnEliminar;
    }

}
