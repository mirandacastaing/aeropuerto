package castaing.miranda.gui;

import castaing.miranda.ui.Main;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.net.URL;
import java.util.ResourceBundle;

public class RegistrarMiembroTripulacion implements Initializable {

    @FXML
    private TextField nombre, apellidos, identificacion, correo, nacionalidad, telefono, numeroLicencia, annosExperiencia;

    @FXML
    private DatePicker fechaGraduacion;

    @FXML
    private ChoiceBox<String> generos, puestos, lineasAereas, tripulaciones;

    @FXML
    private TextArea direccion;

    double x = 0, y = 0;

    @FXML
    void dragged(MouseEvent event) {
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.setX(event.getScreenX() - x);
        stage.setY(event.getScreenY() - y);
    }

    @FXML
    void pressed(MouseEvent event) {
        x = event.getSceneX();
        y = event.getSceneY();
    }

    @FXML
    void registrarUsuario(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("RegistrarUsuario.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void registrarMiembroTripulacion(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("RegistrarMiembroTripulacion.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void registrarAdministrador(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("RegistrarAdministradorMenu.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void enviarInfoMiembroTripulacion(MouseEvent event) throws Exception {
        if (!Main.verificarUsuario(correo.getText(), identificacion.getText())) {
            Main.recibirInfoMiembroTripulacion(nombre.getText(), apellidos.getText(), identificacion.getText(),
                    correo.getText(), direccion.getText(), nacionalidad.getText(), generos.getValue().charAt(0),
                    Integer.parseInt(annosExperiencia.getText()), fechaGraduacion.getValue().toString(),
                    numeroLicencia.getText(), puestos.getValue().charAt(0), telefono.getText(),
                    lineasAereas.getValue(), tripulaciones.getValue());
            iniciarSesion(event);
        }
    }

    @FXML
    void iniciarSesion(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("IniciarSesion.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        String[][] infoLineasAereas = Main.enviarListaLineasAereas();
        String[][] infoTripulaciones = Main.enviarListaTripulaciones();

        listaGeneros.add("Masculino");
        listaGeneros.add("Femenino");
        generos.setItems(listaGeneros);
        generos.getSelectionModel().selectFirst();
        listaPuestos.add("Asistente de vuelo");
        listaPuestos.add("Piloto");
        listaPuestos.add("Copiloto");
        puestos.setItems(listaPuestos);
        puestos.getSelectionModel().selectFirst();
        for (int i = 0; i < infoLineasAereas.length; i++) {
            listaLineasAereas.add(infoLineasAereas[i][2]);
        }
        lineasAereas.setItems(listaLineasAereas);
        lineasAereas.getSelectionModel().selectFirst();
        for (int i = 0; i < infoTripulaciones.length; i++) {
            if (infoTripulaciones[i][2].equals(lineasAereas.getValue())) {
                listaTripulaciones.add(infoTripulaciones[i][1]);
            }
        }
        tripulaciones.setItems(listaTripulaciones);
        tripulaciones.getSelectionModel().selectFirst();
        lineasAereas.getSelectionModel()
                .selectedItemProperty()
                .addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                    listaTripulaciones.clear();
                    for (int i = 0; i < infoTripulaciones.length; i++) {
                        if (infoTripulaciones[i][2].equals(lineasAereas.getValue())) {
                            listaTripulaciones.add(infoTripulaciones[i][1]);
                        }
                    }
                    tripulaciones.setItems(listaTripulaciones);
                    tripulaciones.getSelectionModel().selectFirst();});
    }

    private ObservableList<String> listaGeneros = FXCollections.observableArrayList();
    private ObservableList<String> listaPuestos = FXCollections.observableArrayList();
    private ObservableList<String> listaLineasAereas = FXCollections.observableArrayList();
    private ObservableList<String> listaTripulaciones = FXCollections.observableArrayList();

}
