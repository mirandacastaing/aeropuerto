package castaing.miranda.gui;

import castaing.miranda.ui.Main;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ListaLineasAereas {

    private SimpleStringProperty logo;
    private SimpleStringProperty cedula;
    private SimpleStringProperty nombreComercial;
    private SimpleStringProperty pais;
    private SimpleStringProperty nombreEmpresa;
    private Button btnEditar;
    private Button btnEliminar;

    public ListaLineasAereas(String logo, String cedula, String nombreComercial, String pais, String nombreEmpresa) {
        this.logo = new SimpleStringProperty(logo);
        this.cedula = new SimpleStringProperty(cedula);
        this.nombreComercial = new SimpleStringProperty(nombreComercial);
        this.pais = new SimpleStringProperty(pais);
        this.nombreEmpresa = new SimpleStringProperty(nombreEmpresa);
        this.btnEditar = new Button("Editar");
        this.btnEliminar = new Button("Eliminar");
        this.btnEditar.setStyle("-fx-border-radius: 100px; -fx-background-color: transparent; -fx-text-fill: white; -fx-border-color: white; " +
                "-fx-border-width: 1px; -fx-cursor: hand;");
        this.btnEditar.setId(cedula);
        this.btnEditar.setOnMouseClicked(event -> {
            try {
                ModificarLineaAerea.cedulaLineaAerea = this.btnEditar.getId();
                modificarLineaAerea(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        this.btnEliminar.setStyle("-fx-border-radius: 100px; -fx-background-color: transparent; -fx-text-fill: white; -fx-border-color: white; " +
                "-fx-border-width: 1px; -fx-cursor: hand;");
        this.btnEliminar.setId(cedula);
        this.btnEliminar.setOnMouseClicked(event -> {
            try {
                Main.eliminarLineaAerea(this.btnEliminar.getId());
                listarLineasAereas(event);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @FXML
    void modificarLineaAerea(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ModificarLineaAerea.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    @FXML
    void listarLineasAereas(MouseEvent event) throws Exception {
        ((Node) (event.getSource())).getScene().getWindow().hide();
        Stage window = new Stage();
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("ListarLineasAereas.fxml")));
        scene.setFill(Color.TRANSPARENT);
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.show();
    }

    public String getLogo() {
        return logo.get();
    }

    public SimpleStringProperty logoProperty() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo.set(logo);
    }

    public String getCedula() {
        return cedula.get();
    }

    public SimpleStringProperty cedulaProperty() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula.set(cedula);
    }

    public String getNombreComercial() {
        return nombreComercial.get();
    }

    public SimpleStringProperty nombreComercialProperty() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial.set(nombreComercial);
    }

    public String getPais() {
        return pais.get();
    }

    public SimpleStringProperty paisProperty() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais.set(pais);
    }

    public String getNombreEmpresa() {
        return nombreEmpresa.get();
    }

    public SimpleStringProperty nombreEmpresaProperty() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa.set(nombreEmpresa);
    }

    public Button getBtnEditar() {
        return btnEditar;
    }

    public void setBtnEditar(Button btnEditar) {
        this.btnEditar = btnEditar;
    }

    public Button getBtnEliminar() {
        return btnEliminar;
    }

    public void setBtnEliminar(Button btnEliminar) {
        this.btnEliminar = btnEliminar;
    }

}
