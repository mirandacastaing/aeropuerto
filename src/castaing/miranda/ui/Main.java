package castaing.miranda.ui;

import castaing.miranda.excepciones.AeropuertoException;
import castaing.miranda.tl.Controller;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.sql.SQLException;

public class Main extends Application {

    static Controller gestor = new Controller();

    public static void main(String... args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            if (gestor.verificarAdministradorAeropuertoRegistrados()) {
                Parent fxml = FXMLLoader.load(getClass().getResource("../gui/IniciarSesion.fxml"));
                Scene scene = new Scene(fxml);
                scene.setFill(Color.TRANSPARENT);
                primaryStage.setScene(scene);
                primaryStage.initStyle(StageStyle.TRANSPARENT);
                primaryStage.show();
            } else {
                Parent fxml = FXMLLoader.load(getClass().getResource("../gui/RegistrarAdministradorPrincipio.fxml"));
                Scene scene = new Scene(fxml);
                scene.setFill(Color.TRANSPARENT);
                primaryStage.setScene(scene);
                primaryStage.initStyle(StageStyle.TRANSPARENT);
                primaryStage.show();
            }
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static String recibirCredenciales(String correo, String contrasenna) {
        String fxml = " ";
        try {
            fxml = gestor.verificarCredenciales(correo, contrasenna);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return fxml;
    }

    public static String enviarNombreCompletoPersonaIngresada() {
        return gestor.obtenerNombreCompletoPersonaIngresada();
    }

    public static void recibirInfoUsuario(String nombre, String apellidos, String identificacion, String correo,
                                          String direccion, String nacionalidad, String provincia,
                                          String canton, String distrito, String fechaNacimiento) {
        try {
            gestor.procesarUsuario(nombre, apellidos, identificacion, correo, direccion, nacionalidad, provincia,
                    canton, distrito, fechaNacimiento);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void recibirInfoAdministrador(String nombre, String apellidos, String identificacion, String correo,
                                          String direccion, String nacionalidad, String fechaNacimiento, String genero) {
        try {
            gestor.procesarAdministrador(nombre, apellidos, identificacion, correo, direccion, nacionalidad,
                    fechaNacimiento, genero);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void recibirInfoMiembroTripulacion(String nombre, String apellidos, String identificacion,
                                                     String correo, String direccion, String nacionalidad, char genero,
                                                     int annosExperiencia, String fechaGraduacion, String
                                                             numeroLicencia, char puesto, String telefono, String
                                                             nombreComercialLineaAerea, String nombreTripulacion) {
        try {
            gestor.procesarMiembroTripulacion(nombre, apellidos, identificacion, correo, direccion, nacionalidad, genero,
                    annosExperiencia, fechaGraduacion, numeroLicencia, puesto, telefono, nombreComercialLineaAerea,
                    nombreTripulacion);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void recibirInfoAeropuertoPais(String nombreAeropuerto, String codigoAeropuerto, String codigoPais,
                                                 String nombrePais, String abreviaturaPais,
                                                 String correoAdministrador) {
        try {
            gestor.procesarAeropuertoPais(nombreAeropuerto, codigoAeropuerto, codigoPais, nombrePais, abreviaturaPais,
                    correoAdministrador);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void recibirInfoAeropuerto(String nombreAeropuerto, String codigoAeropuerto, String nombrePais,
                                                 String correoAdministrador) {
        try {
            gestor.procesarAeropuerto(nombreAeropuerto, codigoAeropuerto, nombrePais, correoAdministrador);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void recibirInfoUbicacion(char planta, String descripcion) {
        try {
            gestor.procesarUbicacion(planta, descripcion);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void actualizarInfoUbicacion(char planta, String descripcion) {
        try {
            gestor.actualizarUbicacion(planta, descripcion);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void eliminarUbicacion(char planta) {
        try {
            gestor.borrarUbicacion(planta);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void recibirInfoSala(String nombre, String codigo, char planta) {
        try {
            gestor.procesarSala(nombre, codigo, planta);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void actualizarInfoSala(String nombre, String codigo, char planta) {
        try {
            gestor.actualizarSala(nombre, codigo, planta);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void eliminarSala(String codigo) {
        try {
            gestor.borrarSala(codigo);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void recibirInfoPais(String codigo, String nombre, String abreviatura) {
        try {
            gestor.procesarPais(codigo, nombre, abreviatura);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void actualizarInfoPais(String codigo, String nombre, String abreviatura) {
        try {
            gestor.actualizarPais(codigo, nombre, abreviatura);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void eliminarPais(String codigo) {
        try {
            gestor.borrarPais(codigo);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void recibirInfoLineaAerea(String nombreComercial, String cedula, String nombreEmpresa,
                                             String nombrePais, String logo) {
        try {
            gestor.procesarLineaAerea(nombreComercial, cedula, nombreEmpresa, nombrePais, logo);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void actualizarInfoLineaAerea(String nombreComercial, String cedula, String nombreEmpresa,
                                                String nombrePais, String logo) {
        try {
            gestor.actualizarLineaAerea(nombreComercial, cedula, nombreEmpresa, nombrePais, logo);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void eliminarLineaAerea(String cedula) {
        try {
            gestor.borrarLineaAerea(cedula);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void recibirInfoAeronave(String nombreComercialLineaAerea, String placa, String marca, String modelo,
                                           int capacidad) {
        try {
            gestor.procesarAeronave(nombreComercialLineaAerea, placa, marca, modelo, capacidad);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void actualizarInfoAeronave(String nombreComercialLineaAerea, String placa, String marca,
                                              String modelo, int capacidad) {
        try {
            gestor.actualizarAeronave(nombreComercialLineaAerea, placa, marca, modelo, capacidad);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void eliminarAeronave(String placa) {
        try {
            gestor.borrarAeronave(placa);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void recibirInfoVuelo(String nombreComercialLineaAerea, String nombreAeropuertoOrigen,
                                        String horaLlegada, String horaSalida, String nombreAeropuertoDestino,
                                        char tipo, String placaAeronave, String nombreSala, String nombreTripulacion,
                                        String numeroVuelo, String fechaLlegada, String fechaSalida, double precio) {
        try {
            gestor.procesarVuelo(nombreComercialLineaAerea, nombreAeropuertoOrigen, horaLlegada, horaSalida,
                    nombreAeropuertoDestino, tipo, placaAeronave, nombreSala, nombreTripulacion, numeroVuelo,
                    fechaLlegada, fechaSalida, precio);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void actualizarInfoVuelo(String nombreComercialLineaAerea, String nombreAeropuertoOrigen,
                                           String horaLlegada, String horaSalida, String nombreAeropuertoDestino,
                                           char tipo, String placaAeronave, String nombreSala, String nombreTripulacion,
                                           String numeroVuelo, char estado, String fechaLlegada, String fechaSalida,
                                           double precio) {
        try {
            gestor.actualizarVuelo(nombreComercialLineaAerea, nombreAeropuertoOrigen, horaLlegada, horaSalida,
                    nombreAeropuertoDestino, tipo, placaAeronave, nombreSala, nombreTripulacion, numeroVuelo, estado,
                    fechaLlegada, fechaSalida, precio);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void eliminarVuelo(String numero) {
        try {
            gestor.borrarVuelo(numero);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void recibirInfoTripulacion(String nombreComercialLineaAerea, String codigo, String nombre) {
        try {
            gestor.procesarTripulacion(nombreComercialLineaAerea, codigo, nombre);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void actualizarInfoTripulacion(String nombreComercialLineaAerea, String codigo, String nombre) {
        try {
            gestor.actualizarTripulacion(nombreComercialLineaAerea, codigo, nombre);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void eliminarTripulacion(String codigo) {
        try {
            gestor.borrarTripulacion(codigo);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static void recibirInfoImpuesto(double impuesto) {
        try {
            gestor.procesarImpuesto(impuesto);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static boolean verificarUsuario(String correo, String identificacion) {
        boolean existe = false;
        try {
            existe = gestor.verificarUsuario(correo, identificacion);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return existe;
    }

    public static boolean verificarVuelo(String numeroVuelo) {
        boolean existe = false;
        try {
            existe = gestor.verificarVuelo(numeroVuelo);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return existe;
    }

    public static boolean verificarAeronave(String placa) {
        boolean existe = false;
        try {
            existe = gestor.verificarAeronave(placa);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return existe;
    }

    public static boolean verificarLineaAerea(String cedula, String nombreComercial) {
        boolean existe = false;
        try {
            existe = gestor.verificarLineaAerea(cedula, nombreComercial);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return existe;
    }

    public static boolean verificarSala(String codigo, String nombre) {
        boolean existe = false;
        try {
            existe = gestor.verificarSala(codigo, nombre);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return existe;
    }

    public static boolean verificarUbicacion(char planta) {
        boolean existe = false;
        try {
            existe = gestor.verificarUbicacion(planta);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return existe;
    }

    public static boolean verificarPais(String codigo, String nombre) {
        boolean existe = false;
        try {
            existe = gestor.verificarPais(codigo, nombre);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return existe;
    }

    public static boolean verificarTripulacion(String codigo, String nombre, String lineaAerea) {
        boolean existe = false;
        try {
            existe = gestor.verificarTripulacion(codigo, nombre, lineaAerea);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return existe;
    }

    public static void recibirInfoTiquete(String numeroAsiento, double precio, String numeroVuelo, char tipoAsiento) {
        try {
            gestor.procesarTiquete(numeroAsiento, precio, numeroVuelo, tipoAsiento);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
    }

    public static String[][] enviarListaUbicaciones() {
        String[][] infoUbicaciones = null;
        try {
            infoUbicaciones = gestor.listarUbicaciones();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return infoUbicaciones;
    }

    public static String[][] enviarListaSalas() {
        String[][] info = null;
        try {
            info = gestor.listarSalas();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return info;
    }

    public static String[][] enviarListaPaises() {
        String[][] info = null;
        try {
            info = gestor.listarPaises();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return info;
    }

    public static String[][] enviarListaLineasAereas() {
        String[][] info = null;
        try {
            info = gestor.listarLineasAereas();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return info;
    }

    public static String[][] enviarListaAeronaves() {
        String[][] info = null;
        try {
            info = gestor.listarAeronaves();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return info;
    }

    public static String[][] enviarListaVuelos() {
        String[][] info = null;
        try {
            info = gestor.listarVuelos();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return info;
    }

    public static String[][] enviarListaVuelosLlegada() {
        String[][] info = null;
        try {
            info = gestor.listarVuelosLlegada();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return info;
    }

    public static String[][] enviarListaVuelosSalida() {
        String[][] info = null;
        try {
            info = gestor.listarVuelosSalida();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return info;
    }

    public static String[][] enviarListaTripulaciones() {
        String[][] info = null;
        try {
            info = gestor.listarTripulaciones();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return info;
    }

    public static String[][] enviarListaVuelosMiembroTripulacion() {
        return gestor.listarVuelosMiembroTripulacion();
    }

    public static String[][] enviarListaAeropuertos() {
        String[][] info = null;
        try {
            info = gestor.listarAeropuertos();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return info;
    }

    public static String[] enviarInfoUbicacion(char planta) {
        String[] infoUbicacion = null;
        try {
            infoUbicacion = gestor.getUbicacion(planta);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return infoUbicacion;
    }

    public static String[] enviarInfoSala(String codigo) {
        String[] infoSala = null;
        try {
            infoSala = gestor.getSala(codigo);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return infoSala;
    }

    public static String[] enviarInfoPais(String codigo) {
        String[] infoPais = null;
        try {
            infoPais = gestor.getPais(codigo);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return infoPais;
    }

    public static String[] enviarInfoLineaAerea(String cedula) {
        String[] infoLineaAerea = null;
        try {
            infoLineaAerea = gestor.getLineaAerea(cedula);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return infoLineaAerea;
    }

    public static String[] enviarInfoAeronave(String placa) {
        String[] infoAeronave = null;
        try {
            infoAeronave = gestor.getAeronave(placa);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return infoAeronave;
    }

    public static String[] enviarInfoTripulacion(String codigo) {
        String[] infoTripulacion = null;
        try {
            infoTripulacion = gestor.getTripulacion(codigo);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return infoTripulacion;
    }

    public static String[] enviarInfoVuelo(String numero) {
        String[] infoVuelo = null;
        try {
            infoVuelo = gestor.getVuelo(numero);
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return infoVuelo;
    }

    public static String enviarImpuestoAeropuerto() {
        String infoImpuestoAeropuerto = null;
        try {
            infoImpuestoAeropuerto = gestor.getImpuestoAeropuerto();
        } catch (Exception e) {
            AeropuertoException exc = new AeropuertoException(e.getMessage());
            System.out.println(exc.numeroToString());
        }
        return infoImpuestoAeropuerto;
    }

}
